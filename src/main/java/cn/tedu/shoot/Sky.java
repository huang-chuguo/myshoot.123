package cn.tedu.shoot;

import java.awt.image.BufferedImage;

public  class Sky extends FlyingObject{
    private int speed;
    protected int y1;
    public Sky() {
        super(1300, 3000,0,0);
        speed=1;
        y1=-3000;
    }
    public void step() {
        y+=speed;
        y1+=speed;
        if (y>=World.HEIGHT){
            y=-World.HEIGHT;
        }
        if (y1>=World.HEIGHT){
            y1=-World.HEIGHT;
        }
    }

    public BufferedImage getImage() {
        return Images.sky;
    }

    @Override
    public boolean isOutofBounds() {
        return false;
    }


    public int getY1(){
        return y1;
    }
}
