package cn.tedu.shoot;

import java.awt.image.BufferedImage;
import java.util.Random;

public class Airplane extends FlyingObject implements EnemyScore{
    private int speed;

    public Airplane() {
        super(100, 106);
        Random ran=new Random();
        speed=ran.nextInt(8);
    }



    public void step() {
        y+=speed;
    }
    private int index=1;
    public BufferedImage getImage() {
        if (isLife()) {
            return Images.airs[0];
        } else if (isDead()) {
            BufferedImage img = Images.airs[index++];

            if (index == Images.airs.length) {
                state = REMOVE;
            }
            return img;
        }
            return null;
    }
    public boolean isOutofBounds() {
        return y>World.HEIGHT;
    }
    public int getSocre() {
        return 1;
    }
}
