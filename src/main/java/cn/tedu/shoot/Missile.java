package cn.tedu.shoot;

import java.awt.image.BufferedImage;
import java.util.Random;

public class Missile extends FlyingObject{
    private int xspeed;
    private  int yspeed;
    public Missile(int x, int y) {
        super(16, 18,x,y);
        xspeed=2;
        yspeed=2;

    }

    @Override
    public void step() {
        Random rand=new Random();

        y += yspeed;
        xspeed=rand.nextInt(2);
        if (x <= 0 || x >= World.WIDTH - width) {
            xspeed *= -1;
        }

    }

    @Override
    public BufferedImage getImage() {
        if (isLife()){
            return Images.missile;
        }else if (isDead()){
            state=REMOVE;
        }
        return null;
    }
    public void goDean(){
        state=DEAD;
    }

    @Override
    public boolean isOutofBounds() {
        return y>World.HEIGHT;
    }
}
