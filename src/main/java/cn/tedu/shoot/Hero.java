package cn.tedu.shoot;

import java.awt.image.BufferedImage;

public class Hero extends FlyingObject {
    private int life;
    private int fire;
    public Hero() {
        super(107, 90,World.WIDTH/2,World.HEIGHT/2);
        life=3;
        fire=0;
    }
    public void step() {
        System.out.println("英雄机切换图片啦");
    }
    private int index=0;

    public BufferedImage getImage() {
        //    return Images.heros[index++%Images.heros.length];
        if (fire > 0) {
            return Images.heros[1];
        } else {
            return Images.heros[0];

        }
    }
    @Override
    public boolean isOutofBounds() {
        return false;
    }

    public Bullet[] shoot() {
        int xStep = this.width / 4;
        int yStep = 20;
        if (fire > 0) {
            Bullet[] bs = new Bullet[3];
            bs[0] = new Bullet(this.x + 0 * xStep, this.y - yStep);
            bs[1] = new Bullet(this.x + 1* xStep, this.y - yStep);
            bs[2] = new Bullet(this.x + 2 * xStep, this.y - yStep);
            fire -= 2;
            return bs;
        } else {
            Bullet[] bs = new Bullet[1];
            bs[0] = new Bullet(this.x + 1 * xStep, this.y - yStep);
            return bs;
        }
    }
    public void moveTo(int x,int y){
        this.x=x-this.width/2;
        this.y=y-this.height/2;
    }
    public void addLife(){
        if(life<20){
        life++;
        }
    }
    public int getLife(){
        return life;
    }
    public void subtractLife(){
        life--;
    }
    public void addFire(){
        if(fire<=960) {
            fire += 40;
        }
    }
    public void clearFire(){
        fire=0;
    }

}
