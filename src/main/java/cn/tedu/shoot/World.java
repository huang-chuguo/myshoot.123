package cn.tedu.shoot;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;
import java.util.Timer;

public class World extends JPanel {
    public static final int WIDTH =1000;
    public static final int HEIGHT =800;
    public static final int START=0;
    public static final int RUNNING=1;
    public static final int PAUSE=2;
    public static final int GAME_OVER=3;
    private int state=START;
    private Hero hero=new Hero();
    private Sky sky=new Sky();
    private FlyingObject[]enemies={};
    private Bullet[]bts={};
    private Boss boss = null;
    private Missile[]mis={};

    public FlyingObject nextOne(){
        Random rand=new Random();
        int type=rand.nextInt(100);
        if (type<30){
            return new Bee();
        }else if (type<65){
            return new Airplane();
        }else if(type<90){
            return new BigAirplane();
        }else {
//            return new Boss();
            return new Bee();
        }
    }
    private int enterIndex=0;
    public void enterAction() {
        enterIndex++;
        if (enterIndex % 40== 0) {
            FlyingObject obj = nextOne();
            enemies = Arrays.copyOf(enemies, enemies.length + 1);
            enemies[enemies.length - 1] = obj;
        }
    }
    private int shootIndex=0;
    public void shootAction(){
        shootIndex++;
        if(shootIndex%30==0){
            Bullet bs[]=hero.shoot();
            bts=Arrays.copyOf(bts,bts.length+bs.length);
            System.arraycopy(bs,0,bts,bts.length-bs.length,bs.length);

        }
    }
    private int launchIndex=0;
    public void launchAction() {
        launchIndex++;
       if (boss!=null) {
            if (launchIndex % 60 == 0) {
//                Missile[] ms = boss.launch();
////                System.out.println(ms.length+"--------------------");
//                mis = Arrays.copyOf(mis, mis.length + ms.length);
//                System.arraycopy(ms, 0, mis, mis.length - ms.length, ms.length);
                Missile ms = boss.launch();
                mis = Arrays.copyOf(mis,mis.length+1);
                mis[mis.length-1] = ms;
            }
        }

       }

    public void stepAction(){
        sky.step();
        if(boss!=null)
            boss.step();
        for (int i=0;i<enemies.length;i++){
            enemies[i].step();
        }
        for (int i=0;i<bts.length;i++){
            bts[i].step();
        }
        
            for (int i = 0; i < mis.length; i++) {
                mis[i].step();
            }
        
    }
    public void outOfBoundsAction() {
        for (int i=0;i<enemies.length;i++) {

            if (enemies[i].isOutofBounds()|| enemies[i].isRemove()){
                enemies[i] = enemies[enemies.length - 1];
                enemies = Arrays.copyOf(enemies, enemies.length - 1);
            }
        }
        for (int i=0;i<bts.length;i++){
            if (bts[i].isOutofBounds()||bts[i].isRemove()){
                bts[i]=bts[bts.length-1];
                bts=Arrays.copyOf(bts,bts.length-1);
            }
        }
        if (boss!=null){
        for (int i = 0; i < mis.length; i++) {
            if (mis[i].isOutofBounds()) {
                mis[i] = mis[mis.length - 1];
                mis = Arrays.copyOf(mis, mis.length - 1);
            }
        }

        }
    }




    private int score=0;
    public void bulletBangAction(){
        for (int i=0;i<bts.length;i++){
            Bullet b=bts[i];
            for(int j=0;j<enemies.length;j++){
                FlyingObject f=enemies[j];
                if (b.isLife()&&f.isLife()&&f.ishit(b)){
                    b.goDean();
                    f.goDean();

                    if (f instanceof EnemyScore){
                        EnemyScore es=(EnemyScore)f;
                        score+=es.getSocre();
                    }
                    if (f instanceof EnemyAward){
                        EnemyAward ea=(EnemyAward)f;
                        int type=ea.getAwardType();
                        switch (type){
                            case EnemyAward.FIRE:
                            hero.addFire();
                            break;
                            case EnemyAward.LIVE:
                            hero.addLife();
                            break;
                        }
                    }
                }
            }
        }
    }
    public void missileBangAction() {
        if (boss!=null){
        for (int i = 0; i < mis.length; i++) {
            Missile m = mis[i];
            if (m.isLife() && hero.isLife() && m.ishit(hero)) {
                m.goDean();
                hero.subtractLife();
                hero.clearFire();

            }
        }
        }
    }
    public void heroBangAction(){
        for (int i=0;i<enemies.length;i++){
            FlyingObject f=enemies[i];
            if(hero.isLife()&&f.isLife()&&f.ishit(hero)) {
                f.goDean();
                hero.subtractLife();
                hero.clearFire();
            }
        }

    }
    public void bossBangAction(){
        for (int i=0;i<bts.length;i++){
            Bullet b=bts[i];
            if(boss.isLife()&&b.isLife()&&b.ishit(boss)) {
                b.goDean();
                boss.subtractLife();
            }

        }
    }
    public void checkGameOverAction(){
        if (hero.getLife()<=0){
            state=GAME_OVER;
        }
    }
    public void bossoverAction(){
        if (boss.getLife()<=0){
            boss.goDean();
            mis = Arrays.copyOf(mis,0);
        }
    }

    public void action(){
        final MouseAdapter m=new MouseAdapter() {
            public void mouseMoved(MouseEvent e) {
                if (state==RUNNING){
                    int x=e.getX();
                    int y=e.getY();
                    hero.moveTo(x,y);
                }if(state!=GAME_OVER&&state!=START){
                    if(e.getX()>=WIDTH){
                        state=PAUSE;
                    }else if(e.getX()<=WIDTH){
                        state=RUNNING;
                    }}
            }
            public void mouseClicked(MouseEvent e) {
                switch (state){
                    case START:
                        state=RUNNING;
                        break;
                    case GAME_OVER:
                        state=START;
                        score=0;
                        hero=new Hero();
                        sky=new Sky();
                        enemies=new FlyingObject[0];
                        bts=new Bullet[0];
                        boss = null;
                        mis = new Missile[0];
                        break;
                }
            }
            public void mouseEntered(MouseEvent e) {
                if (state==PAUSE){
                    state=RUNNING;
                }
            }

            public void mouseExited(MouseEvent e) {
                if (state==RUNNING){
                    state=PAUSE;
                }
            }

        };
        this.addMouseListener(m);
        this.addMouseMotionListener(m);
        Timer timer=new Timer();
        int intervel=10;
        timer.schedule(new TimerTask() {
            public void run() {
                if (state==RUNNING){
                    enterAction();
                    shootAction();
                    stepAction();
                    outOfBoundsAction();
                    bulletBangAction();
                    heroBangAction();
                    checkGameOverAction();
                    missileBangAction();
                    launchAction();
                    if (boss!=null) {
                        bossBangAction();
                        bossoverAction();
                    }
                }
                repaint();
            }
        },intervel,intervel);
        thread.setDaemon(true);
        thread.start();
    }
    Thread thread = new Thread(){
        @Override
        public void run() {
            try {
                while (true) {
                    Thread.sleep(20000);
                    boss = new Boss();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    };


    public void paint(Graphics g) {
        g.drawImage(sky.getImage(), sky.x, sky.y, null);
        g.drawImage(sky.getImage(), sky.x, sky.y1, null);
        g.drawImage(hero.getImage(), hero.x, hero.y, null);
        hero.paint(g);
        if(boss!=null)
            boss.paint(g);
        for (int i = 0; i < enemies.length; i++) {
            FlyingObject f = enemies[i];
            g.drawImage(f.getImage(), f.x, f.y, null);
        }
        for (int i = 0; i < bts.length; i++) {
            Bullet b = bts[i];
            g.drawImage(b.getImage(), b.x, b.y, null);
        }

            for (int i = 0; i < mis.length; i++) {
                Missile m = mis[i];
                g.drawImage(m.getImage(), m.x, m.y, null);
            }


        String date=String.valueOf(new Date());
        g.setColor(new Color(255, 255, 255));
        g.fillRect(WIDTH,0,200,HEIGHT);
        g.setColor(new Color(0, 0, 0));
        g.drawString("Fire："+hero.getLife(),WIDTH+82,HEIGHT-70);
        g.drawString("LIFE："+hero.getLife(),WIDTH+12,HEIGHT-70);
        g.setFont(new Font("宋体",Font.BOLD,30));
        g.drawString("SCORE:"+score,WIDTH+10,45);
        g.drawString("Date:"+date,10,HEIGHT-50);
        g.setColor(new Color(255, 200, 200));
        g.fillRect(WIDTH + 10,HEIGHT - 200,50,100);
        g.setColor(new Color(255, 0, 0));
        for(int i=0;i<hero.getLife()+1;i++) {
            g.fillRect(WIDTH + 10,HEIGHT - 100 - i * 5,50,5);

        }
        g.setColor(new Color(255, 200, 200));
        g.fillRect(WIDTH + 70,HEIGHT - 200,50,100);
        g.setColor(new Color(0, 0, 255));
        for(int i=0;i<(hero.getLife())/40+1;i++){
            g.fillRect(WIDTH + 70,HEIGHT - 100 - i * 5,50,5);
        }
        switch (state) {

            case START:
                g.drawImage(Images.start, 300, 0, null);
                break;
            case PAUSE:
                g.drawImage(Images.pause, 300, 150, null);
                break;
            case GAME_OVER:
                g.drawImage(Images.gameover, 300, 250, null);
                break;
        }
    }

    private void remove(Missile[] mis) {
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        World world = new World();
        frame.add(world);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(WIDTH+200, HEIGHT);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        world.action();
    }
}










