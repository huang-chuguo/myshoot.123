package cn.tedu.shoot;

import java.awt.image.BufferedImage;
import java.util.Random;

public class BigAirplane extends FlyingObject implements EnemyScore {
    private int speed;

    public BigAirplane() {
        super(50, 81);
        Random ran=new Random();
        speed =ran.nextInt(8);
    }

    public void step() {
        y += speed;
    }

    private int index = 1;

    public BufferedImage getImage() {
        if (isLife()) {
            return Images.bairs[0];

        } else if (isDead()) {
            BufferedImage img = Images.bairs[index++];
            if (index ==Images.bairs.length) {
                state=REMOVE;
            }
            return img;
        }
        return null;
        }

    @Override
    public boolean isOutofBounds() {
        return y>World.HEIGHT;
    }

    @Override
    public int getSocre() {
        return 3;
    }
}