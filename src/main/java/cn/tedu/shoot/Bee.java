package cn.tedu.shoot;

import java.awt.image.BufferedImage;
import java.util.Random;

public class Bee extends FlyingObject implements EnemyAward{
    private int xspeed;
    private int yspeed;
    private int awardType;

    public Bee() {
        super(62, 100);
        Random rand = new Random();


        xspeed = rand.nextInt(8);;
        yspeed = 3;
        awardType = rand.nextInt(2);
    }

    public void step() {

        x+=xspeed;

        y += yspeed;

        if (x <= 0 || x >= World.WIDTH - width) {
            xspeed *= -1;
        }
    }

    private int index = 1;

    public BufferedImage getImage() {
        if (isLife()) {
            return Images.bees[0];
        } else if (isDead()) {
            BufferedImage img = Images.bees[index++];
            if (index == Images.bees.length) {
                state = REMOVE;
            }
            return img;
        }
        return null;

    }

    @Override
    public boolean isOutofBounds() {
        return y>World.HEIGHT;
    }

    public int getAwardType() {
        return awardType;
    }
}