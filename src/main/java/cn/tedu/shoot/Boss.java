package cn.tedu.shoot;

import java.awt.image.BufferedImage;
import java.util.Random;

public class Boss extends FlyingObject implements EnemyAward,EnemyScore{
    private int xspeed;
    private int awardType;
    private int life;
    public Boss() {
        super(251, 231);
        Random rand=new Random();
        x=rand.nextInt(World.WIDTH-width);
        y=0;
        xspeed=1;
        awardType = rand.nextInt(2);
        life=50;
    }

    @Override
    public int getAwardType() {
        return awardType;
    }

    @Override
    public int getSocre() {
        return 5;
    }

    @Override
    public void step() {
        x+=xspeed;
        if (x <= 0 || x >= World.WIDTH - width) {
            xspeed *= -1;
        }

    }


    private int index=1;
    public BufferedImage getImage() {
        if (isLife()) {
            return Images.boss[0];
        } else if (isDead()) {
//            BufferedImage img = Images.boss[index++];
//
//            if (index == Images.boss.length) {
//                state = REMOVE;
//            }
//            return img;
            state = REMOVE;
        }
        return null;
    }
    public Missile launch(){

            int xStep = this.width / 4;
            int yStep = 20;
            Missile ms;
            ms = new Missile(this.x + 2 * xStep, this.y + height - yStep);
            return ms;


    }
    @Override
    public boolean isOutofBounds() {
        return false;
    }


    public void subtractLife() {
    life--;
    }

    public int getLife(){
        return life;
    }

}
