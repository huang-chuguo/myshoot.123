package cn.tedu.shoot;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;

public class Images {
    public static BufferedImage sky;
    public static BufferedImage bullet;
    public static BufferedImage missile;
    public static BufferedImage []boss;
    public static BufferedImage []heros;
    public static BufferedImage []airs;
    public static BufferedImage []bairs;
    public static BufferedImage []bees;
    public static BufferedImage start;  //启动
    public static BufferedImage pause; //暂停
    public static BufferedImage gameover;//结束




    static { //初始化静态资源
        start=readImage("start.png");
        pause=readImage("pause.png");
        gameover=readImage("gameover.png");


        sky=readImage("background.png");
        bullet=readImage("bullet.png");
        missile=readImage("missile.png");


        heros=new BufferedImage[2];
        heros[0]=readImage("hero0.png");
        heros[1]=readImage("hero1.png");

        airs=new BufferedImage[5];
        bairs=new BufferedImage[5];
        bees=new BufferedImage[5];
        boss=new BufferedImage[5];

        airs[0]=readImage("airplane.png");
        bairs[0]=readImage("bigairplane.png");
        bees[0]=readImage("Bee.png");
        boss[0]=readImage("boss.png");


        for(int i=1;i<airs.length;i++) {
            airs[i]=readImage("bom"+i+".png");
            bairs[i]=readImage("bom"+i+".png");
            bees[i]=readImage("bom"+i+".png");
            boss[i]=readImage("bom"+i+".png");

        }

    }
    /**读取图片  fileName ：图片名称*/
    public static BufferedImage readImage(String fileName){
        try{
   //         BufferedImage img = ImageIO.read(FlyingObject.class.getResource(fileName));
           BufferedImage img = ImageIO.read(new File("./src/main/java/cn/tedu/shoot/"+fileName));
            return img;
        }catch(Exception e){
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

}
